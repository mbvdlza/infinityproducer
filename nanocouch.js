
"use strict";

var config = require('./config');
var log = require('./log');
var Q = require('q');
var db;

function NanoCouch(host, port, user, pass, name) {
	log.debug('NanoCouch Construction %s @ %s', user, host);
	this.host = host;
	this.port = port;
	this.user = user;
	this.pass = pass;
	this.name = name;
	this.nano = false;
}

NanoCouch.prototype.connect = function() {
	log.info('NanoCouch Connection.');
	var authString = 'http://' + this.user + ':' + this.pass;
	authString += '@' + this.host + ':' + this.port;
	log.debug('AuthString: %s', authString);
	this.nano = require('nano')(authString);
}

NanoCouch.prototype.insertChunk = function(chunk) {
	log.debug('NanoCouch insertChunk');
	var deferred = Q.defer();

	this.nano.use(this.name).insert(chunk, function(err, body) {
    if (err) {
			log.debug('Deffered.reject', err);
      deferred.reject(new Error(err));
    } else {
			log.debug('Deffered.resolved', body);
      deferred.resolve(body);
    }
  });

  return deferred.promise;
}

db = new NanoCouch(
	config.db.couchdb.host,
	config.db.couchdb.port,
	config.db.couchdb.user,
	config.db.couchdb.pass,
	config.db.couchdb.db
);

module.exports = db;
