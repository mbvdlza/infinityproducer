
"use strict";

var winston = require('winston');
var config = require('./config');

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ level: 'debug', colorize: true}),
    new (winston.transports.File)({ level: 'info', filename: config.sys.logfile })
  ],
  //exceptionHandlers: [
    //new winston.transports.File({ filename: config.sys.exceptlogfile, maxsize:128000 })
  //],
  exitOnError: false
});

logger.debug('Winston Logger is alive!');
module.exports = logger;
