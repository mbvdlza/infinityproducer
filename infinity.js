
"use strict";

var config = require('./config');
var db = require('./database');
var log = require('./log');
var http;
var currentChunk; // perhaps https://www.npmjs.com/package/binary-format

function Infinity(server) {
  log.debug('Instantiating Infinity with server');
  http = server;
}

/**
 * Retrieve a chunk at address x,y from the database, if it exists.
 * If it does not exist, generation of a new chunk is triggerred.
 */
Infinity.prototype.getChunk = function(x, y) {

}

/**
 * Handles procedural generation of a chunk of space.
 * Should this be handled asynchronously?
 */
Infinity.prototype.generateChunk = function() {

}

/**
 * The socket.io power delivery function.
 * This will send back the chunk, whether it has been
 * generated or simply retrieved.
 */
Infinity.prototype.deliverChunk = function() {

}

Infinity.prototype.getToken = function() {
  log.debug('getToken()');
}




var aChunkOfSpace = {
  "token": "hashOfClientIPAndClientSoftware",
  "entity": "chunk",
  "data": {
    "idX": 1,
    "idY": -1,
    "objects": [
      {
        "id": "abc123def567",
        "onChunkPositionX": 500,
        "onChunkPositionY": 112,
        "onAngle": 45
      },
      {
        "id": "678fff123edd",
        "onChunkPositionX": 210,
        "onChunkPositionY": 220,
        "onAngle": 45
      }
    ]
  }
}

db.connect();
db.test()
   .then(function(body) {
     log.debug('then... body ', body);
   });
log.debug('done inserting.');


// db.insertChunk(aChunkOfSpace)
//   .then(function(body) {
//     log.debug('then... body ', body);
//   });
//   log.debug('done inserting.');

module.exports = Infinity;
