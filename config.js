var config = {};

config.db = {};
config.web = {};
config.sys = {};

config.web.title =  'Proliferate Service';
config.web.port = 9669;

config.db.couchdb = {};
config.db.couchdb.host = 'localhost';
config.db.couchdb.db = 'InfinityProducer';
config.db.couchdb.user = 'marlon';
config.db.couchdb.pass = 'blackadder';
config.db.couchdb.port = 6677;

config.sys.loglevel = '';
config.sys.logfile = 'infinity_dev.log';
config.sys.exceptlogfile = 'exceptions.log';



module.exports = config;
