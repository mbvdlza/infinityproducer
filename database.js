
"use strict";

var config = require('./config');
var log = require('./log');
var Q = require('q');
var mysql = require('mysql');
var db;

function Maria(host, port, user, pass, name) {
	log.debug('Maria Construction %s @ %s', user, host);
	this.host = host;
	this.port = port;
	this.user = user;
	this.pass = pass;
	this.name = name;
	this.connection;
	this.id = 'MariaDB';
}

Maria.prototype.connect = function() {
	log.info('%s Connection.', this.id);
	this.connection = mysql.createConnection({
  	host     : this.host,
  	user     : this.user,
  	password : this.pass,
  	database : this.name
	});
	this.connection.connect();
}

/**
 * A simple database test function, I promise.
 */
Maria.prototype.test = function() {
	var deferred = Q.defer();
	this.connection.query('SELECT 1 + 1 AS solution', function(err, rows, fields) {
		if (err) {
			log.debug('Deffered.reject', err);
			deferred.reject(new Error(err));
		} else {
			log.debug('Deffered.resolved', rows[0].solution);
			deferred.resolve(rows[0].solution);
		}
	});
	this.connection.end();
	return deferred.promise;
}

Maria.prototype.insertChunk = function(chunk) {
	log.debug('%s insertChunk', this.id);
}

db = new Maria(
	config.db.couchdb.host,
	config.db.couchdb.port,
	config.db.couchdb.user,
	config.db.couchdb.pass,
	config.db.couchdb.db
);

module.exports = db;
