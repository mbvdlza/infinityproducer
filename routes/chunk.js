var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('InfinityProducer Chunk Service');
});

module.exports = router;
